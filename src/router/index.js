import Vue from "vue";
import VueRouter from "vue-router";
import Navbar from '@/components/Navbar.vue'
import User from '@/views/management/User.vue'
import Hospital from '@/views/management/Hospital.vue'
import Package from '@/views/management/Package.vue'
import Summary from '@/views/management/Summary.vue'
import API from '@/views/management/API.vue'
import Announcement from '@/views/management/Announcement.vue'
import AdminOneID from '@/views/management/AdminOneID.vue'
import Client from '@/views/management/Client.vue'
import UserOneID from '@/views/management/UserOneID.vue'
// import VueCookies from 'vue-cookies'

Vue.use(VueRouter);

const routes = [
  { path: "*", redirect: "/management/user" },
  {
    path: "/navbar",
    name: "Navbar",
    component: Navbar,
    // meta: { user: true },
    children: [
      {
        path: "/management/user",
        name: "User",
        // meta: { user: true },
        component: User,
      },
      {
        path: "/management/hospital",
        name: "Hospital",
        // meta: { user: true },
        component: Hospital,
      },
      {
        path: "/management/package",
        name: "Package",
        // meta: { user: true },
        component: Package,
      },
      {
        path: "/management/summary",
        name: "Summary",
        // meta: { user: true },
        component: Summary,
      },
      // {
      //   path: "/management/summary",
      //   name: "Summary",
      //   // meta: { user: true },
      //   component: NewSummary,
      // },
      {
        path: "/management/api",
        name: "API",
        // meta: { user: true },
        component: API,
      },
      {
        path: "/management/announcement",
        name: "Announcement",
        // meta: { user: true },
        component: Announcement,
      },
      {
        path: "/management/adminoneid",
        name: "AdminOneID",
        // meta: { user: true },
        component: AdminOneID,
      },
      {
        path: "/management/client",
        name: "Client",
        component: Client,
      },
      {
        path: "/management/useroneid",
        name: "UserOneID",
        component: UserOneID,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.guest)) {
//     const checkcookie = VueCookies.get('info_login')
//     console.log(checkcookie);  
//     if (!checkcookie) {
//       next()
//     } else if (checkcookie.role === 'superadmin') {
//       next({ name: 'User' })
//     } else if (checkcookie.role === 'user') {
//       next({ name: 'Upload' })
//     } else if (checkcookie.role === 'viewer') {
//       next({ name: 'History' })
//     }
//   } else if (to.matched.some(record => record.meta.user)) {
//     const checkcookie = VueCookies.get('info_login')
//     if (!checkcookie) {
//       next({ name: 'Login' })
//     } else {
//       next()
//     }
//   } else {
//     next()
//   }
// })

export default router;
